import ROOT
from sl3offsets import sl3_ofssets

def print_tree(input_file, output_file):
    f = ROOT.TFile.Open(input_file)
    with open(output_file, "w") as out:
        for event in f.DTTree:
            for elem in zip(event.digi_wheel, event.digi_sector, event.digi_station, event.digi_sl, event.digi_layer, event.digi_wire, event.digi_time):
                shift = sl3_ofssets[(elem[0], elem[1], elem[2])]
                out.write("{} {} {} {} {} {} {} {}\n".format(elem[0], elem[1], elem[2], shift / 10., elem[3] - 1, elem[4] - 1, elem[5], int(round(elem[6]))))
            out.write("-1 -1 -1 -1 -1 -1 -1 -1\n")

def print_emulator(input_file, output_file):
    f = ROOT.TFile.Open(input_file)
    tree = f.Get("dtNtupleProducer/DTTREE")
    with open(output_file, "w") as out:
        for iev, event in enumerate(tree):
            for elem in zip(event.ph2TpgPhiEmuAm_quality, event.ph2TpgPhiEmuAm_posLoc_x, event.ph2TpgPhiEmuAm_dirLoc_phi,
                    event.ph2TpgPhiEmuAm_t0, event.ph2TpgPhiEmuAm_phi, event.ph2TpgPhiEmuAm_phiB, event.ph2TpgPhiEmuAm_chi2,
                    event.ph2TpgPhiEmuAm_wheel, event.ph2TpgPhiEmuAm_sector, event.ph2TpgPhiEmuAm_station,
                    event.ph2TpgPhiEmuAm_superLayer,
                    event.ph2TpgPhiEmuAm_pathWireId, event.ph2TpgPhiEmuAm_pathTDC, event.ph2TpgPhiEmuAm_pathLat):
                out.write(" ".join([str(e) for e in elem[0:11]]))
                for i in range(8):
                    out.write(" %s" % elem[11][i])
                for i in range(8):
                    out.write(" %s" % elem[12][i])
                for i in range(8):
                    out.write(" %s" % elem[13][i])
                out.write("\n")
            out.write("-1\n")


if __name__ == "__main__":
    print_hits("/eos/user/j/jleonhol/public_ntuples/rossin_noRPC_noAgeing_ext_confok_alignTrue.root", "hits.txt")
    print_emulator("/eos/user/j/jleonhol/public_ntuples/rossin_noRPC_noAgeing_ext_confok_alignTrue.root", "emu.txt")
